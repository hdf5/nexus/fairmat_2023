# Fairmat 2023

https://hdf5.gitlab-pages.esrf.fr/nexus/fairmat_2023

Submitted to the official *NeXus standard*:

https://github.com/nexusformat/definitions/pull/1271
